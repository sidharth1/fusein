package filemanager;

import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;

import java.io.FileOutputStream;
import java.io.IOException;

import main.RemoteDrive;
import main.RemoteFile;
import main.RemoteFolder;
import services.Dropbox;

/**
 * A file on the Dropbox remote server.
 *
 * @author Ryan K
 * @date March 17, 2014
 */
public class DropboxFile implements RemoteFile {
    private RemoteFolder parent;
    private DbxEntry.File dbxFile;
    private final Dropbox dbox;

    public DropboxFile(DbxEntry.File dbxFile, RemoteFolder parent, final Dropbox dbox) {
        this.dbox = dbox;
        this.parent = parent;
        this.dbxFile = dbxFile;
    }

    @Override
    public String getName() {
        return this.dbxFile.name;
    }

    @Override
    public String getPath() {
        return this.dbxFile.path;
    }

    @Override
    public String getLink() {
        try {
            return dbox.getClient().createShareableUrl(this.getPath());
        } catch (DbxException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public RemoteDrive getRemoteDrive() {
        return dbox;
    }

    @Override
    public RemoteFolder getFolder() {
        return this.parent;
    }

    @Override
    public boolean download(String localPath) {
        FileOutputStream outputStream = null;
        // Retrieve getClient(), open file stream.
        try {
            outputStream = new FileOutputStream(localPath);
            // Fully write the file to local system.
            dbox.getClient().getFile(this.getPath(), null, outputStream);
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean delete() {
        try {
            dbox.getClient().delete(this.getPath());
        } catch (DbxException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean isFolder() {
        return false;
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    public RemoteFolder asFolder() {
        return null;
    }

    @Override
    public RemoteFile asFile() {
        return this;
    }
    
}
