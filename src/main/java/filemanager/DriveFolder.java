package filemanager;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.ParentReference;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.RemoteDrive;
import main.RemoteEntry;
import main.RemoteFile;
import main.RemoteFolder;
import services.GDrive;

/**
 * A folder on the Google Drive remote server.
 *
 * @author Ryan K
 * @date March 05, 2014
 */
public class DriveFolder implements RemoteFolder {
    private RemoteFolder parent;
    private com.google.api.services.drive.model.File driveFolder;
    private final GDrive drive;
    
    public DriveFolder(com.google.api.services.drive.model.File driveFolder, RemoteFolder parent, final GDrive drive) {
        this.drive = drive;
        this.parent = parent;
        this.driveFolder = driveFolder;
    }
    
    @Override
    public String getName() {
        return this.driveFolder.getTitle();
    }
    
    @Override
    public String getPath() {
        return this.driveFolder.getId();
    }
    
    @Override
    public RemoteDrive getRemoteDrive() {
        return drive;
    }
    
    @Override
    public RemoteFolder getParent() {
        return this.parent;
    }
    
    @Override
    public List<RemoteEntry> getEntries() {
        Drive.Children.List request = null;
        try {
            request = drive.getDriveClient().children().list(this.getPath());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return fillEntries(request);
    }
    
    public List<RemoteEntry> fillEntries(Drive.Children.List request) {
        ArrayList<RemoteEntry> entries = new ArrayList<>();
        do {
            try {
                ChildList children = request.execute();
                for (ChildReference child : children.getItems()) {
                    String childId = child.getId();
                    com.google.api.services.drive.model.File file = drive.getDriveClient().files().get(childId).execute();
                    if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
                        entries.add(new DriveFolder(file, this, drive));
                    } else {
                        entries.add(new DriveFile(file, this, drive));
                    }
                }
                request.setPageToken(children.getNextPageToken());
            } catch (IOException e) {
                System.out.println("An error occurred: " + e);
                request.setPageToken(null);
            }
        } while (request.getPageToken() != null && request.getPageToken().length() > 0);
        return entries;
    }
    
    @Override
    public boolean isFolder() {
        return true;
    }
    
    @Override
    public boolean isFile() {
        return false;
    }
    
    @Override
    public RemoteFolder asFolder() {
        return this;
    }
    
    @Override
    public RemoteFile asFile() {
        return null;
    }
    
    @Override
    public RemoteFile uploadFile(String localPath) {
        String path = localPath;
        File inputFile = null;
        if (localPath != null) {
            inputFile = new File(path);
        } else {
            return null;
        }
        Path uploadPath = inputFile.toPath();
        String mimeType = "";
        try {
            mimeType = java.nio.file.Files.probeContentType(uploadPath);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return null;
        }
        // File's metadata.
        com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
        body.setTitle(inputFile.getName());
        body.setDescription("Uploaded from fusein");
        body.setMimeType(mimeType);
        // Set the parent folder.
        body.setParents(Arrays.asList(new ParentReference().setId(this.getPath())));
        // File's content.
        FileContent mediaContent = new FileContent(mimeType, inputFile);
        try {
            com.google.api.services.drive.model.File file = drive.getDriveClient().files().insert(body, mediaContent).execute();
            System.out.println("File was uploaded successfully");
            RemoteFile uploadedFile = new DriveFile(file, this, drive);
            return uploadedFile;
        } catch (IOException e) {
            System.out.println("An error occured: " + e);
            return null;
        }
    }
    
}
