package filemanager;

import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxWriteMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import main.RemoteDrive;
import main.RemoteEntry;
import main.RemoteFile;
import main.RemoteFolder;
import services.Dropbox;

/**
 * A folder on the Dropbox remote server.
 *
 * @author Ryan K
 * @date March 17, 2014
 */
public class DropboxFolder implements RemoteFolder {
    private RemoteFolder parent;
    private DbxEntry.Folder dbxFolder;
    private final Dropbox dbox;

    public DropboxFolder(DbxEntry.Folder dbxFolder, RemoteFolder parent, final Dropbox dbox) {
        this.dbox = dbox;
        this.parent = parent;
        this.dbxFolder = dbxFolder;
    }

    @Override
    public String getName() {
        return this.dbxFolder.name;
    }

    @Override
    public String getPath() {
        return this.dbxFolder.path;
    }

    @Override
    public RemoteDrive getRemoteDrive() {
        return dbox;
    }

    @Override
    public RemoteFolder getParent() {
        return this.parent;
    }

    @Override
    public List<RemoteEntry> getEntries() {
        DbxEntry.WithChildren listing = null;
        // Now get the file listing information from the account.
        try {
            listing = dbox.getClient().getMetadataWithChildren(this.getPath());
        } catch (DbxException e) {
            e.printStackTrace();
            return null;
        }
        return fillEntries(listing);
    }

    public List<RemoteEntry> fillEntries(DbxEntry.WithChildren listing) {
        ArrayList<RemoteEntry> entries = new ArrayList();
        for (DbxEntry child : listing.children) {
            if (child.isFile()) {
                entries.add(new DropboxFile(child.asFile(), this, dbox));
            } else {
                entries.add(new DropboxFolder(child.asFolder(), this, dbox));
            }
        }
        return entries;
    }

    @Override
    public RemoteFile uploadFile(String localPath) {
        FileInputStream inputStream = null;
        // Retrieve the specified file from the local system.
        File inputFile = new File(localPath);
        try {
            inputStream = new FileInputStream(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        // Now upload it to the authenticated Dropbox account.
        try {
            DbxEntry.File uploadedFile = dbox.getClient().uploadFile(this.getPath() + "/" + inputFile.getName(), DbxWriteMode.add(), inputFile.length(), inputStream);
            return new DropboxFile(uploadedFile, this, dbox);
        } catch (IOException | DbxException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean isFolder() {
        return true;
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    public RemoteFolder asFolder() {
        return this;
    }

    @Override
    public RemoteFile asFile() {
        return null;
    }
    
}
