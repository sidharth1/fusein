package filemanager;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import main.RemoteDrive;
import main.RemoteFile;
import main.RemoteFolder;
import services.GDrive;

/**
 * A file on the Google Drive remote server.
 *
 * @author Ryan K
 * @date March 05, 2014
 */
public class DriveFile implements RemoteFile {
    private RemoteFolder parent;
    private com.google.api.services.drive.model.File driveFile;
    private final GDrive drive;

    public DriveFile(com.google.api.services.drive.model.File driveFile, RemoteFolder parent, final GDrive drive) {
        this.drive = drive;
        this.parent = parent;
        this.driveFile = driveFile;
    }

    @Override
    public String getName() {
        return this.driveFile.getTitle();
    }

    @Override
    public String getPath() {
        return this.driveFile.getId();
    }

    @Override
    public String getLink() {
        String url = driveFile.getAlternateLink();
        return url;
    }

    @Override
    public RemoteDrive getRemoteDrive() {
        return drive;
    }

    @Override
    public RemoteFolder getFolder() {
        return this.parent;
    }

    @Override
    public boolean download(String localPath) {
        InputStream is = null;
        String remoteFilePath = this.getPath();
        HashMap<String, String> mimeTypes = new HashMap<String, String>();
        mimeTypes.put("application/vnd.google-apps.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mimeTypes.put("application/vnd.google-apps.spreadsheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        mimeTypes.put("application/vnd.google-apps.drawing", "application/pdf");
        mimeTypes.put("application/vnd.google-apps.presentation", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        com.google.api.services.drive.model.File file = null;
        try {
            file = drive.getDriveClient().files().get(remoteFilePath).execute();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return false;
        }
        String downloadUrl = "";
        String mimeType = "";
        if (file.getDownloadUrl() == null) {
            mimeType = file.getMimeType();
            mimeType = mimeTypes.get(mimeType);
            downloadUrl = file.getExportLinks().get(mimeType);
        } else {
            downloadUrl = file.getDownloadUrl();
        }
        System.out.println(downloadUrl);
        if (downloadUrl != null && downloadUrl.length() > 0) {
            try {
                HttpResponse resp = drive.getDriveClient().getRequestFactory().buildGetRequest(new GenericUrl(downloadUrl)).execute();
                is = resp.getContent();
            } catch (IOException e) {
                // An error occurred.
                e.printStackTrace();
                return false;
            }
        } else {
            // The file doesn't have any content stored on Drive
            System.out.println("test");
            return false;
        }
        java.io.File localFile = new java.io.File(localPath);
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(localFile);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];
        try {
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            byte[] arr = buffer.toByteArray();
            is.read(arr);
            os.write(arr);
            os.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isFolder() {
        return false;
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    public RemoteFolder asFolder() {
        return null;
    }

    @Override
    public RemoteFile asFile() {
        return this;
    }

    @Override
    public boolean delete() {
        try {
            drive.getDriveClient().files().delete(this.getPath()).execute();
        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
            return false;
        }
        return true;
    }
    
}
