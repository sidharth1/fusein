package services;

import filemanager.DriveFolder;
import java.io.IOException;
import java.util.Arrays;


import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;

import main.RemoteDrive;
import main.RemoteFolder;

/**
 * Provides an implementation of the basic cloud storage functionality for a
 * Google Drive account. All functionality will require a proper authentication token
 * to be issued by the Drive authentication server.
 * 
 * @author Fusein
 * @date March 06, 2014
 * @version 1.0
 */
public class GDrive extends CommonServices implements RemoteDrive {
	private String redirectUri;
	private HttpTransport httpTransport;
	private JsonFactory jsonFactory;
	private GoogleCredential credential;
	private GoogleAuthorizationCodeFlow flow;
	private String authToken;
	private Drive service;
	private About info; 

	/**
	 * Default constructor to initialize data
	 */
	public GDrive() {
                this.setOptions("428540166150-r7chn8m9gb7o9m3vkkibmbbs444999i1.apps.googleusercontent.com",
                        "kJzGYz02Nog18vnkkyIOQ0kI",
                        "FuseIn");
		redirectUri = "urn:ietf:wg:oauth:2.0:oob";
		httpTransport = new NetHttpTransport();
		jsonFactory = new JacksonFactory();
	}
	
	@Override
	public String getServiceNiceName()
	{
		return super.getNiceName(1);
	}
	
	@Override
	public void generateAuthURL() {
		flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory, super.getAppKey(), super.getAppSecret(), Arrays.asList(DriveScopes.DRIVE)).setAccessType("offline").setApprovalPrompt("force").build();

		String url = flow.newAuthorizationUrl().setRedirectUri(redirectUri).build();

		super.openAuthLink(url);
	}
	
	@Override
	public boolean finalizeAuth(String authKey) {
		GoogleTokenResponse response = null;
		try {
			response = flow.newTokenRequest(authKey).setRedirectUri(redirectUri).execute();
		} catch (IOException e) {
			System.out.println("Unable to authorize. Please enter a valid key.");
			return false;
		}

		this.credential = new GoogleCredential.Builder().setJsonFactory(jsonFactory)
	            .setTransport(httpTransport).setClientSecrets(super.getAppKey(), super.getAppSecret()).build();
		credential.setFromTokenResponse(response);

		// Create a new authorized API client
		this.service = new Drive.Builder(httpTransport, jsonFactory, credential).setApplicationName("FuseIn").build();
		
		try {
			this.info = service.about().get().execute();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		this.authToken = this.credential.getAccessToken();
		
		return true;
	}
	
	public void setApiInfo(){
		try {
			this.info = service.about().get().execute();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	@Override
	public String getUsername()
	{
		return this.info.getName();
	}
	
	@Override
	public String getAuthToken()
	{
		return this.authToken;
	}
	
	public String getRootFolderID(){
		return this.info.getRootFolderId();
	}
	
	@Override
	public void setAuthToken(String newToken)
	{
		this.authToken = newToken;
		
		// Get our new client object
		this.credential = new GoogleCredential().setAccessToken(newToken);
		this.service = new Drive.Builder(httpTransport, jsonFactory, credential).setApplicationName("FuseIn").build();
		try {
			this.info = this.service.about().get().execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}

	@Override
	public RemoteFolder getRootFolder() {
		try {
			com.google.api.services.drive.model.File rootFolder = service.files().get(this.info.getRootFolderId()).execute();
			return new DriveFolder(rootFolder, null, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Drive getService(){
		return this.service;
	}
	
	public void setService(Drive service){
		this.service = service;
	}
        
        public Drive getDriveClient(){
            return this.service;
        }
}
