
package services;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;



/**
 *
 * @author sid
 */
public class CommonServices {
    private String appKey, appSecret, clientIdentifier, userLocale;
    
    public void setOptions(String appKey, String appSecret, String clientIdentifier){
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.clientIdentifier = clientIdentifier;
        this.userLocale = Locale.getDefault().toString();
    }
    
    public String getAppKey() {
        return this.appKey;
    }
    
    public String getAppSecret() {
        return this.appSecret;
    }
    
    public String getClientIdentifier() {
        return this.clientIdentifier;
    }
    
    public String getUserLocale() {
        return this.userLocale;
    }
    
    public String getNiceName(int typeOfService){
        if(typeOfService == 0){
            return "Dropbox";
        }
        if(typeOfService == 1){
            return "Google Drive";
        }
        return null;
    }
    /**
     * Opens the account authorization link in the user's
     * default browser.
     *
     * @param authorizeUrl The URL to open.
     */  
    public void openAuthLink(String authorizeUrl) {
        try {
            Desktop.getDesktop().browse(new URL(authorizeUrl).toURI());
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }
    
    
}
