package services;

import filemanager.DropboxFolder;
import main.RemoteDrive;
import main.RemoteFolder;

import com.dropbox.core.DbxAccountInfo;
import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;

/**
 * Provides an implementation of the basic cloud storage functionality for a
 * Dropbox account. All functionality will require a proper authentication token
 * to be issued by the Dropbox authentication server.
 *
 * @author Fusein
 * @date March 17, 2014
 * @version 2.0
 */
public class Dropbox extends CommonServices implements RemoteDrive {
    private String authToken;
    private DbxClient client;
    private DbxAccountInfo info;
    private DbxWebAuthNoRedirect webAuth;
    private DbxAppInfo appInfo;
    private DbxRequestConfig requestConfig;
    
    /**
     * Default constructor to initialize data
     */
    public Dropbox() {
        this.setOptions("7qtdwzgsnts24wm", "5dbvc8pxaxn6d61", "fusein/0.1");
    }
    
    @Override
    public String getServiceNiceName()
    {
        return super.getNiceName(0);
    }
    
    @Override
    public void generateAuthURL() {
        // Confirm app as registered by Dropbox.
        appInfo = new DbxAppInfo(super.getAppKey(), super.getAppSecret());
        
        // Defaults to locale: English.
        requestConfig = new DbxRequestConfig(super.getClientIdentifier(), super.getUserLocale());
        webAuth = new DbxWebAuthNoRedirect(requestConfig, appInfo);
        
        // Fetch the authorization authorization url properly.
        String authorizeUrl = webAuth.start();
        super.openAuthLink(authorizeUrl);
    }
    
    @Override
    public boolean finalizeAuth(String authKey) {
        DbxAuthFinish authFinish = null;
        try {
            authFinish = webAuth.finish(authKey);
        } catch (DbxException e) {
            System.out.println("Unable to authorize. Please enter a valid key.");
            return false;
        }
        
        this.authToken = authFinish.accessToken;
        DbxRequestConfig requestConfig = new DbxRequestConfig(super.getClientIdentifier(), super.getUserLocale());
        this.client = new DbxClient(requestConfig, this.authToken);
        try {
            this.info = this.client.getAccountInfo();
        } catch (DbxException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    @Override
    public String getUsername()
    {
        return this.info.displayName;
    }
    
    @Override
    public String getAuthToken()
    {
        return this.authToken;
    }
    
    @Override
    public void setAuthToken(String newToken)
    {
        this.authToken = newToken;
        
        // Get our new client object
        DbxRequestConfig requestConfig = new DbxRequestConfig(super.getClientIdentifier(), super.getUserLocale());
        this.client = new DbxClient(requestConfig, this.authToken);
        try {
            this.info = this.client.getAccountInfo();
        } catch (DbxException e) {
            e.printStackTrace();
            return;
        }
    }
    
    @Override
    public RemoteFolder getRootFolder() {
        try {
            return new DropboxFolder(this.client.getMetadata("/").asFolder(), null, this);
        } catch (DbxException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public DbxClient getClient(){
        return this.client;
    }
}
